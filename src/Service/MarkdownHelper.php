<?php

namespace App\Service;

use Knp\Bundle\MarkdownBundle\MarkdownParserInterface;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Security;

class MarkdownHelper
{
    private $markdown;

    private $cache;

    private $logger;

    private $security;

    public function __construct(MarkdownParserInterface $markdown, AdapterInterface $cache, LoggerInterface $markdownLogger, Security $security)
    {
        $this->markdown = $markdown;
        $this->cache = $cache;
        $this->logger = $markdownLogger;
        $this->security = $security;
    }

    public function parse(string $source): string
    {
        if(strpos($source, 'bacon') !== false) {
            $this->logger->info('bacon mention', ['user' => $this->security->getUser()]);
        }

        $item = $this->cache->getItem('markdown_'.md5($source));
        if (!$item->isHit()) {
            $item->set($this->markdown->transform($source));
            $this->cache->save($item);
        }
        return $item->get();
    }
}