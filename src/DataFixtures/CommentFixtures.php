<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use App\Entity\Article;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class CommentFixtures extends BaseFixture implements DependentFixtureInterface
{
    public function getDependencies()
    {
        return [ArticleFixtures::class];
    }

    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(Comment::class, 100, function(Comment $comment){
            $comment->setContent(
                $this->faker->boolean() ? $this->faker->paragraph() : $this->faker->sentences(2, true)
            );
            $comment->setAuthorName($this->faker->name());
            $comment->setCreatedAt($this->faker->dateTimeBetween('-1 months', '-1 seconds'));
            $comment->setArticle($this->getReference(Article::class.'_'.$this->faker->numberBetween(0, 9)));
            $comment->setIsDeleted($this->faker->boolean(20));
        });

        $manager->flush();
    }
}
