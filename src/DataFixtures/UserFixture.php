<?php

namespace App\DataFixtures;


use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\ApiToken;

class UserFixture extends BaseFixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(User::class, 10, function(User $user, $i) use ($manager) {
            $user->setEmail('spacebar'. $i .'@example.com');
            $user->setFirstName($this->faker->firstName);
            $user->setPassword($this->passwordEncoder->encodePassword($user, 'samesame'));
            $user->agreeTerms();
            if($this->faker->boolean()) {
                $user->setTwitterUsername($this->faker->userName);
            }

            $apiToken1 = new ApiToken($user);
            $apiToken2 = new ApiToken($user);

            $manager->persist($apiToken1);
            $manager->persist($apiToken2);

        });

        $this->createMany(User::class, 3, function(User $user, $i) {
            $user->setEmail('admin'. $i .'@space.com');
            $user->setFirstName($this->faker->firstName);
            $user->setRoles(['ROLE_ADMIN']);
            $user->setPassword($this->passwordEncoder->encodePassword($user, 'samesame'));
            $user->agreeTerms();
            if($this->faker->boolean()) {
                $user->setTwitterUsername($this->faker->userName);
            }
        }, 10);

        $manager->flush();
    }
}
