<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Article;
use App\Repository\ArticleRepository;

class ArticleController extends AbstractController 
{
    /**
	 * @Route("/", name="article_index", methods={"GET"})
	 */
    public function index(ArticleRepository $repository)
    {
        //$articles = $repository->findBy([], ['publishedAt' => 'DESC']);
        $articles = $repository->findAllPublishedOrderedByNewest();

        return $this->render('article/homepage.html.twig', ['articles' => $articles]);
    }

    /**
     * @Route("/news/{slug}", name="article_show")
     */
    public function show(Article $article)
    {
        // $repository = $em->getRepository(Article::class);
        // /** @var Article $article */
        // $article = $repository->findOneBy(['slug' => $slug]);

        // if(!$article) {
        //     throw $this->createNotFoundException('Article not found:' . $slug);
        // }

        //$comments = $article->getComments();

        return $this->render('article/show.html.twig', [
            'article' => $article,
            //'comments' => $comments,
        ]);
    }

    /**
     * @Route("news/{slug}/like", name="article_like", methods={"POST"})
     */
    public function like(Article $article, LoggerInterface $logger, EntityManagerInterface $em)
    {
        $logger->info('article liked');
        $article->incrementLikeCount();
        $em->flush();

        return new JsonResponse(['hearts' => $article->getLikeCount()]);
    }
}